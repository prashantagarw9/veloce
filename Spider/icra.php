<?php
  include_once('dom.php');
 

  $url = $_GET['query'];
  $html = new simple_html_dom();
  $html->load_file($url);
 ?><html><head>
		<link rel="stylesheet" type="text/css" href="./index_files/bootstrap.css">
		<style>
			

			.imageContainer {
				
				position: relative;
                                alignment-adjust: middle;
			}

			
			div.currentImage {
				display: inline;
      			position: absolute;
                       
			}

			div.oldImage {
				display: none;
                                alignment-adjust: middle;
      			       
			}

			.prev, .next {
				position: absolute;
			}
			
			.prev {
				top: 300px;
				left: 0px;
			}

			.next {
				top: 300px;
				right: 0px;
			}

			
		</style>
	</head>
	<body bgcolor="red">

		<div class="container" >
			<div class="row" >
				
				<div class="span12 imageContainer" >
                                    <div class="currentImage"  ><img src="./index_files/image1.jpg" ></div>
                                    <?php
                               foreach($html->find("img") as $link)
                               {
                               echo "<div class='oldImage'>$link</div>";
                                }

                                 ?>
		            
				</div>
				
			</div>			
		</div>
		<img class="prev" src="./index_files/prev.png">
					
		<img class="next" src="./index_files/next.png">
		<script src="./index_files/jquery.min.js"></script>
  		<script src="./index_files/jquery-ui.min.js"></script>
  
		<script src="./index_files/gesto.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.prev').bind('click', function(){
					var currentImage = $('.currentImage');
					var nextImage = $('.currentImage').prev();
					
					if(nextImage.length) {
						currentImage.hide("slide",{direction: "left"}, 1000).removeClass("currentImage").addClass("oldImage");
						nextImage.removeClass("oldImage").show("slide", {direction: "right"}, 1000).addClass("currentImage");
					}		
				});

				$('.next').bind('click', function() {
					var currentImage = $('.currentImage');
					var previousImage = $('.currentImage').next();

					if(previousImage.length) {
						currentImage.hide("slide",{direction: "right"}, 1000).removeClass("currentImage").addClass("oldImage");
						previousImage.removeClass("oldImage").show("slide", {direction: "left"}, 1000).addClass("currentImage");
					}

				});
			});
		</script>
	

</body></html>
