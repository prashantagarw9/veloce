<!DOCTYPE html>
<html>
<head>
 <style type="text/css">        
h1 { margin-top:5px;
    font-family: "Lobster", sans-serif;
    font-size: 56px;
    font-weight: bold;
    text-align: left;
}

.mask-galaxy {
    background: url('img.jpg') center center;
    -webkit-text-fill-color: transparent;
    -webkit-background-clip: text;
    -webkit-animation: travelGalaxy 10s linear infinite;
    -moz-animation: travelGalaxy 10s linear infinite;
    -moz-text-fill-color: transparent;
    -moz-background-clip: text;
    -moz-animation: travelGalaxy 10s linear infinite;
    -o-text-fill-color: transparent;
    -o-background-clip: text;
    -o-animation: travelGalaxy 10s linear infinite;
}

@-webkit-keyframes travelGalaxy {
    0% {background-position: right bottom;}
    100% {background-position: left top;}
}
@-moz-keyframes travelGalaxy {
    0% {background-position: right bottom;}
    100% {background-position: left top;}
}    
</style> 


  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="gmaps.js"></script>
   <script type="text/javascript" src="annyang.min.js"></script>
           <script>
if (annyang) {
  // Let's define a command.
   var commands = {
    'locate *term': function(term) { document.search.address.value=term; document.getElementById('btn').click()},
    'home' : function() { location.href="../search.php"; },
    'crawl' : function() { location.href="../icrawl.php"; },
    'maps' : function() { location.href="maps.php"; }
  
  };
  

  // Initialize annyang with our commands
  annyang.init(commands);

  // Start listening.
  annyang.start();
}
</script>
 
  <link rel="stylesheet" type="text/css" href="examples.css" />
  <link rel="stylesheet" type="text/css" href="search.css" />
  <script type="text/javascript">
    var map;
    $(document).ready(function(){
      map = new GMaps({
        el: '#map',
        lat: 0,
        lng: 0
      });
      $('#geocoding_form').submit(function(e){
        e.preventDefault();
        GMaps.geocode({
          address: $('#address').val().trim(),
          callback: function(results, status){
            if(status=='OK'){
              var latlng = results[0].geometry.location;
              map.setCenter(latlng.lat(), latlng.lng());
              map.addMarker({
                lat: latlng.lat(),
                lng: latlng.lng()
              });
            }
          }
        });
      });
    });
  </script>
</head>
<body>
   <link href="http://fonts.googleapis.com/css?family=Lobster&subset=latin" rel="stylesheet" type="text/css">

  <div align="center">
      <br><br><br>
  <table width="350">
      <tr><td height="67">
<article class="mask-galaxy">
  <div align="right"><h1 id="sr">VELOCE</h1></div>
  
</article>
 </td>
 <td>
 
  <div class="row">
    <div class="span11">
      <form method="post" id="geocoding_form" name="search">
  
        <div class="input">
        <p class="s">  <input type="text" id="address" name="address" /></p>
          <input type="submit" class="btn" value="Search" id="btn"/>
        </div>
      </form>
      
    </div>
  </div>
  </td></tr></table>
  </div>
  <div id="map"></div>
</body>
</html>
<?php 
include "head.php";
include "footer.php";
?>
