<script language=php>
	// simple data provider for a cascading dropdown

	Header("Content-type: text/xml"); 
	// get query string params
	$parent = $_GET['parent'];
	$level = $_GET['level'];
	
	include("data_settings.php");
	
	//Create query of other level records
	$qry = "SELECT id, descr ";
	$qry .= "FROM ajax_data ";
	$qry .= "WHERE parent = $parent and level = $level";
	$result = mysql_query($qry);

	// build xml content for JavaScript interpreter 
	// note: using the amperdsand (&) in your description field will cause a javascript error
	$xml = "<mxml>";
	while($row = mysql_fetch_array($result)){	
		$id = $row['id'];
		$descr = $row['descr'];
		$xml = $xml . '<parent_level name="'.$parent.'">';
		$xml = $xml . '<child_level id="'.$id.'">'.$descr.'</child_level>';
		$xml = $xml . '</parent_level>';
	}
	$xml = $xml . "</mxml>";	
	// send xml to client
	echo( $xml );
</script>